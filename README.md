# PangeaDAO_Polygonal_Sherpa_Bounty

// precisely hard surface modeled game ready assets<br>
// 3 different colored layouts (based on PangeDAO's brand identity and colorwheel)<br>
// rigged assets with skeletal mesh (anim ready & tested on Babylon.js' sandbox online)<br>
// optimized filesizes provided in both .gltf and .glb formats + collider file for decentraland collision policy<br>
// 4k renders of finished assets for marketing purposes<br>


## previews:
![prev1](https://gitlab.com/milenaveleva/pangeadao_polygonal_sherpa_bounty/-/raw/main/_previews/preview1.png?raw=true)<br>
![prev2](https://gitlab.com/milenaveleva/pangeadao_polygonal_sherpa_bounty/-/raw/main/_previews/preview2.png?raw=true)<br>
![prev3](https://gitlab.com/milenaveleva/pangeadao_polygonal_sherpa_bounty/-/raw/main/_previews/preview3.png?raw=true)<br>
